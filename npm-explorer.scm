;;; Copyright © 2018 Julien Lepiller <julien@lepiller.eu>
;;; Copyright © 2018 swedebugia <swedebugia@riseup.net>
;;;
;;; This file is part of guile-npm-explorer.
;;;
;;; guile-npm-explorer is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guile-npm-explorer is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-npm-explorer.  If not, see <http://www.gnu.org/licenses/>.

;; Usage:
;; $guile -s npm-explorer.scm >mocha.dot (later you pipe these
;; dot-files into graphviz to produce the actual graph.
;;
;; or
;;
;; Do it all at once:
;; guile -s npm-explorer.scm |dot -Tsvg > mocha.svg
;;
;; or
;;
;; Do it all at once and show it with no nonsense in between:
;; guile -s npm-explorer.scm |dot -Tsvg > mocha.svg && eog mocha.svg

(use-modules (guix import json)
	     (guix build utils)
	     (guix import utils)
	     (guix http-client)
	     (srfi srfi-34)
	     (ice-9 rdelim)
	     (ice-9 regex)
	     (ice-9 textual-ports)
	     (json))

;; from
;; https://gitlab.com/swedebugia/guix/blob/08fc0ec6fa76d95f4b469aa85033f1b0148f7fa3/guix/import/npm.scm
;; imported here unchanged because it is not avaliable in upstream guix yet.
(define (node->package-name name)
    "Given the NAME of a package on npmjs, return a Guix-compliant
name for the
package. We remove the '@' and keep the '/' in scoped
packages. E.g. @mocha/test -> node-mocha/test"
    (cond ((and (string-prefix? "@" name)
		(string-prefix? "node-" name))
	   (snake-case (string-drop name 1)))
	  ((string-prefix? "@" name)
	            (string-append "node-" (snake-case (string-drop
							name 1))))
	  ((string-prefix? "node-" name)
	   (snake-case name))
	  (else
	   (string-append "node-" (snake-case name)))))

(define (slash->_ name)
  "Sanitize slashes to avoid cli-problems"
  (if (string-match "[/]" name)
      (regexp-substitute #f (string-match "/+" name)
			 'pre "_slash_" 'post)
      ;;else
      name))

;; FIXME this does not return #f if the file is empty.
(define (read-file file)
  "RETURN hashtable from JSON-file in cache."
  (if (< (stat:size (stat file)) 10)
      ;; size is less than 10 bytes, return #f
      #f
      ;; return file parsed to hashtables with (json)
      (call-with-input-file file
	(lambda (port)
	  (json->scm port)))))

;; from
;; http://git.savannah.gnu.org/cgit/guix.git/tree/guix/import/json.scm
;; adapted to return unaltered JSON
(define* (npm-http-fetch url
                     ;; Note: many websites returns 403 if we omit a
                     ;; 'User-Agent' header.
                     #:key (headers `((user-agent . "GNU Guile")
                                      (Accept . "application/json"))))
  "Return a JSON resource URL, or
#f if URL returns 403 or 404.  HEADERS is a list of HTTP headers to pass in
the query."
  (guard (c ((and (http-get-error? c)
                  (let ((error (http-get-error-code c)))
                    (or (= 403 error)
                        (= 404 error))))
             #f))
    (let* ((port   (http-fetch url #:headers headers))
	      ;; changed the upstream here to return unaltered json:
              (result (get-string-all port)))
      (close-port port)
      result)))

(define (cache-handler name)
  "Check if cached in cache-dir. RETURN direct from cache or fetch and return
from cache."
  (let* ((cache-dir (string-append (getenv "HOME") "/.cache/npm-explorer"))
	 ;; sanitize name to fit in cli-context on disk
	 ;; it can contain @ and /
	 (cache-name (slash->_ (node->package-name name)))
	 (filename (string-append cache-dir "/" cache-name ".package.json")))
    (if (file-exists? filename)
	;;yes
	;;check if empty
	(if (read-file filename)
	    (read-file filename)
	    ;;file empty
	    (begin
	      (format
	       (current-error-port)
	       "cache for ~a was empty, trying to download again..." name)
	      (delete-file filename)
	      ;; call handler again to try fetching again
	      (cache-handler name)))
	;;no
	(begin
	  (when (not (directory-exists? cache-dir))
	    (mkdir-p cache-dir))
	  ;; port closes when this closes
	  (call-with-output-file filename
	    (lambda (port)
	      (format port "~a"
		      ;; this gives os the result-closure and we write it out
		      (npm-http-fetch
		       (string-append
			"https://registry.npmjs.org/"
			name)))))
	  ;; get the content and close
	  (read-file filename)))))

;; FIXME implement returning the latest version if ^ is prefixed to
;; avoid us having to import lots of old versions.
(define (sanitize-version version)
  (cond
   ((string-prefix? "^" version)
    (string-drop version 1))
   ((string-prefix? "*" version)
    (string-drop version 1))
   (else
    version)))

(define (extract-version hashtable version)
  "Return extract from hashtable corresponding to version or #f if not
found."
  (cond
   ((or
     (equal? version "latest")
     (equal? version "*"))
    (let ((latest (hash-ref (hash-ref hashtable "dist-tags") "latest")))
      (hash-ref (hash-ref hashtable "versions") latest)))
   (else
    ;;extract the version specified
    (hash-ref (hash-ref hashtable "versions") version))))

(define (extract-deps hashtable version)
  "Return extract of dependencies from hashtable corresponding to
version or #f if none."
  (cond
   ((or
     (equal? version "latest")
     (equal? version "*"))
    (let* ((latest (hash-ref (hash-ref hashtable "dist-tags") "latest"))
	  (data (hash-ref (hash-ref hashtable "versions") latest)))
      (hash-ref data "dependencies")))
   (else
    ;;extract the version specified
    (let ((data (hash-ref (hash-ref hashtable "versions") version)))
      (hash-ref data "dependencies")))))

;; From Julien.
;; Unaltered besides where noticed
;; Get specific version
(define* (get-npm-module-dot name done level
			     #:optional
			     (version "latest"))
  "RETURN package count and level to std-error and dot-formatted data
to std-out."
  (if (member name done)
      done
      ;; Convert return from cache to hashtable instead of fetching
      ;; everything multiple times for packages with shared dependency
      ;; tails. This results in a significant speedup when file is in
      ;; the cache.
      ;; The cache has no TTL implemented so you should clear it from
      ;; to time manually.
      (let* ((descr (cache-handler name))
	     ;; Sanitize version
	     (version (sanitize-version version))
	     ;; Extract hashtable corresponding to version
	     (extracted-version (extract-version descr version)))
	;; Find the version specified
	(if extracted-version
	    (catch #t
	      ;; Thunk
	      (lambda ()
		(let* (;; Extract deps corresponding to version
	    	       (deps (extract-deps descr version)))
	    	  (if deps
		      ;; Fold through all the elements in the hashtable
	    	      (hash-fold
	    	       (lambda (key value acc)
	    		 (begin
	    		   (format (current-error-port)
	    		   	   "level ~a: ~a packages    \r" level (length acc))
	    		   (let ((value (sanitize-version value)))
	    		     (format #t "\"~a@~a\" -> \"~a@~a\";~%"
				     name version key value))
			   ;; call recursively with the version specified (unsanitized). this
			   ;; results in many more dependencies until
			   ;; the FIXME above is implemented.
	    		   (get-npm-module-dot
			    key acc (+ 1 level) value)))
		       ;; fold recursive
	    	       (cons name done) deps)
		      ;; else, add to done
	    	      (cons name done))))
	      ;; Handler if thunk throws #t
	      ;; not found!
	      (lambda _
		(error
		 (string-append
		  "did not find the data for the last one looked up" " in the registry!"))
		;; do not continue
		;;(cons name done)
		))
	    ;; else
	    (cons name done)))))
  
(format #t "digraph dependencies {~%")
(format #t "overlap=false;~%")
(format #t "splines=true;~%")
(get-npm-module-dot "mocha" '() 0)
(format (current-error-port) "~%")
(format #t "}~%")

;;test
;;(display (slash->_ "babel/mocha")) ;works
;; (display (cache-handler "async")) ;works!
;; (display "fetching")
;; (newline)
;; (display				;works!
;;  (npm-http-fetch
;;   (string-append  "https://registry.npmjs.org/" "mocha")))
;; (display (sanitize-version "^1.3.1")) ;works!
;; (display
;;  (hash-table->alist
;;   (extract-version (cache-handler "json-server") "latest"))) ;works!


