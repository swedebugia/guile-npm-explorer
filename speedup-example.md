First fetch:

sdb@antelope ~/src/guile-npm-explorer$ time guile -s npm-explorer.scm |dot -Tsvg > ssb-p.svg && eog ssb-p.svg
level 0: 535 packages     

real	7m33.364s
user	1m1.511s
sys	0m14.480s

Second with all dependencies in the cache:

sdb@antelope ~/src/guile-npm-explorer$ time guile -s npm-explorer.scm |dot -Tsvg > ssb-p.svg && eog ssb-p.svg
level 0: 535 packages     

real	0m13.368s
user	0m14.716s
sys	0m0.301s

=> 34 times faster!
(this was on a fast 4G broadband in EU, slow hdd 5400 RPM)
